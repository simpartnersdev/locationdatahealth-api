﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

using LDH_Web_API.Models.DataQuery;
using LDH_Web_API.DAL.Token;

namespace LDH_Web_API.BL
{
    public class Listings
    {
        public ListingResponse GetListingsData(ListingRequest req, string Token)
        {
            var resp = new ListingResponse();
            TokenResponse _token;
            try
            {
                _token = Utility.DoValidation(ConfigurationManager.AppSettings["TokenApiURL"], Token);
                if (!_token.Status.Success)
                {
                    throw new Exception(_token.Status.Error);
                }
            }
            catch (Exception ex)
            {
                return new ListingResponse() { Status = new Status(ex.Message) };
            }

            try
            {
                resp = new DAL.AWSRedShiftConnector().GetListings(req, _token.SessionJson.DirectoryID);
            }
            catch (Exception ex)
            {
                resp.Status = new Status(ex.Message);
            }

            return resp;
        }

        public ListingResponse GetProfileScoresData(ProfileScoreRequest req, string Token)
        {
            var resp = new ListingResponse();
            TokenResponse _token;
            try
            {
                _token = Utility.DoValidation(ConfigurationManager.AppSettings["TokenApiURL"], Token);
                if (!_token.Status.Success)
                {
                    throw new Exception(_token.Status.Error);
                }
            }
            catch (Exception ex)
            {
                return new ListingResponse() { Status = new Status(ex.Message) };
            }

            try
            {
                resp = new DAL.AWSRedShiftConnector().GetProfileScores(req, _token.SessionJson.DirectoryID);
            }
            catch (Exception ex)
            {
                resp.Status = new Status(ex.Message);
            }

            return resp;
        }
    }
}