﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using LDH_Web_API.Models.DataQuery;

namespace LDH_Web_API.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class ListingController : ApiController
    {
        /// <summary>
        /// Retrieves the headers allowed by the server
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Options()
        {
            HttpResponseMessage objHttpResponseMessage = new HttpResponseMessage();
            objHttpResponseMessage.StatusCode = HttpStatusCode.OK;
            return objHttpResponseMessage;
        }

        /// <summary>
        /// Returns the metrics for the Listing Data Health process
        /// </summary>
        /// <param name="req">MetricsRequest (query)</param>
        /// <returns>Results of the search include a Summary (agg. calculations) and Details of the grouping in the data, 
        /// if a Group By isntruction was used in the query</returns>
        [HttpPost]
        [Route("api/Listing/GetMetrics")]
        public MetricsResponse GetMetrics(MetricsRequest req)
        {
            string Token;
            try
            {
                Token = Request.Headers.GetValues("Token").FirstOrDefault();
            }
            catch
            {
                return new MetricsResponse() { Status = Status.NoToken() };
            }

            return new BL.Metrics().GetMetricsData(req, Token);
        }

        /// <summary>
        /// Rreturns the details for the listings
        /// </summary>
        /// <param name="req">ListingRequest (query)</param>
        /// <returns>Results of the search include a Summary (agg. calculations) and detailed Profile information, 
        /// including Listings for each Profile. Uses pagination.</returns>
        [HttpPost]
        [Route("api/Listing/GetListings")]
        public ListingResponse GetListings(Models.DataQuery.ListingRequest req)
        {
            string Token;
            try
            {
                Token = Request.Headers.GetValues("Token").FirstOrDefault();
            }
            catch
            {
                return new ListingResponse() { Status = Status.NoToken() };
            }

            return new BL.Listings().GetListingsData(req, Token);
        }

        /// <summary>
        /// Returns a list of Profiles for the Directory ID, allowing you to specify which properties to include in the response
        /// </summary>
        /// <param name="req">ProfileRequest (query)</param>
        /// <returns>List of Profiles for the Directory ID</returns>
        [HttpPost]
        [Route("api/Listing/GetProfiles")]
        public ProfileResponse GetProfiles(Models.DataQuery.ProfileRequest req)
        {
            string Token;
            try
            {
                Token = Request.Headers.GetValues("Token").FirstOrDefault();
            }
            catch
            {
                return new ProfileResponse() { Status = Status.NoToken() };
            }

            return new BL.Profiles().GetProfilesData(req, Token);
        }

        /// <summary>
        /// Rreturns summary and details for the profiles, not including listings
        /// </summary>
        /// <param name="req">ProfileScoreRequest (query)</param>
        /// <returns>Results of the search include a Summary (agg. calculations) and Profile information, 
        /// NOT including Listings for each Profile. Uses pagination.</returns>
        [HttpPost]
        [Route("api/Listing/GetProfileScores")]
        public ListingResponse GetProfileScores(Models.DataQuery.ProfileScoreRequest req)
        {
            string Token;
            try
            {
                Token = Request.Headers.GetValues("Token").FirstOrDefault();
            }
            catch
            {
                return new ListingResponse() { Status = Status.NoToken() };
            }

            return new BL.Listings().GetProfileScoresData(req, Token);
        }
    }
}
