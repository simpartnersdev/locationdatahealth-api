﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models
{
    public class Polygon
    {
        public long? ID { get; set; }
        public string AreaName { get; set; }
        public Micromarket MicroMarket { get; set; }
    }
}