﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models
{
    public class Listing
    {
        public string Domain { get; set; }
        public NAPInfo NAPInfo { get; set; }
        public DateTime ProcessDate { get; set; }
        public string URL { get; set; }
        public Metrics Metrics { get; set; }
        public List<string> NoMatchFields { get; set; }
    }
}