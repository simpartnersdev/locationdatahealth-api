﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models
{
    public class GeoLocation
    {
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
    }
}