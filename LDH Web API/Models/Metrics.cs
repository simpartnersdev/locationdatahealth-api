﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models
{
    public class Metrics
    {
        public decimal HealthScore { get; set; }
        public decimal? HealthScoreDelta { get; set; }
        public int? ProfileCount { get; set; }
        public int? ListingCount { get; set; }
        public int? ActualListingsFoundCount { get; set; }
        public int? IssueCount { get; set; }
    }
}