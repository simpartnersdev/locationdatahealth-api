﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models.DataQuery
{
    public enum SortOrder
    {
        //be careful if need to change the default value (currently ASC)
        //as this affects default behaviors on building the queries and 
        //can have unwanted side effects
        ASC,
        DESC,
        NONE
    }
}