﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models.DataQuery
{
    public class Attribute
    {
        public string Dimension { get; set; }
        public string ColumnName { get; set; }
        public List<string> Values { get; set; }
    }
}