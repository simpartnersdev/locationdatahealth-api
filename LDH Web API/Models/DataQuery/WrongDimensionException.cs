﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models.DataQuery
{
    public class WrongDimensionException : Exception
    {
        public string Dimension { get; private set; }
        public string Column { get; private set; }

        public WrongDimensionException(string Dimension, string Column, string Message) : base (Message)
        {
            this.Column = Column;
            this.Dimension = Dimension;
        }

        public WrongDimensionException(string Dimension, string Column, Exception InnerException, string Message) : base(Message, InnerException)
        {
            this.Column = Column;
            this.Dimension = Dimension;
        }
    }
}