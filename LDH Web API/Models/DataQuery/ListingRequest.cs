﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models.DataQuery
{
    public class ListingRequest
    {
        private ListingOrderBy _orderBy;

        public List<Attribute> DimensionFilters { get; set; }
        public PaginationInformation Pagination { get; set; }
        public bool ListingsWithDifferencesOnly { get; set; }
        public ListingOrderBy OrderBy
        {
            get
            {
                //set default value for order by score
                if (_orderBy == null)
                {
                    _orderBy = new ListingOrderBy();
                    _orderBy.Column = ListingOrderColumn.ProfileScore;
                    _orderBy.Ascending = true;
                }
                return _orderBy;
            }
            set
            {
                _orderBy = value;
            }
        }
    }
}