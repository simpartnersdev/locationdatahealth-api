﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models.DataQuery
{
    public class GenericResponse : IAPIResponse
    {
        public Status Status { get; set; }

        public static IAPIResponse GetErrorResponse<T>(string Message) where T: IAPIResponse, new()
        {
            var resp = new T();
            resp.Status = new Status(Message);
            return resp;
        }
            
    }
}