﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models.DataQuery
{
    public class MetricsOrderBy
    {
        public MetricsOrderColumn Column { get; set; }
        public bool Ascending { get; set; }
    }

    public enum MetricsOrderColumn
    {
        IssueCount,
        TotalListingCount,
        ActualListingCount,
        ProfileScore,
        ProfileScoreDelta,
        None
    }
}