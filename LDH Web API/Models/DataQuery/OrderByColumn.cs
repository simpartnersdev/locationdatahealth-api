﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models.DataQuery
{
    public class OrderByColumn
    {
        public string Name { get; set; }
        public bool Ascending { get; set; }
    }
}