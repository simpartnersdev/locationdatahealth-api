﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDH_Web_API.Models.DataQuery
{
    public class MetricsRequest
    {
        private MetricsOrderBy _orderBy;

        public List<Attribute> DimensionFilters { get; set; }
        public GroupBy GroupBy { get; set; }
        public MetricsOrderBy OrderBy
        {
            get
            {
                //set default value for order by score
                //if a specific order by instruction is given, then use that (set ProfileScoreOrderColumn.None)
                if (_orderBy == null && GroupBy != null && (GroupBy.Order != SortOrder.NONE))
                //if (_orderBy == null && GroupBy.Dimension == "dom_domain")
                {
                    _orderBy = new MetricsOrderBy();
                    _orderBy.Column = MetricsOrderColumn.None;
                }
                //if no other order by instruction is given, then use ProfileScoreOrderColumn.Score
                if (_orderBy == null)
                {
                    _orderBy = new MetricsOrderBy();
                    _orderBy.Column = MetricsOrderColumn.ProfileScore;
                    _orderBy.Ascending = true;
                }
                return _orderBy;
            }
            set
            {
                _orderBy = value;
            }
        }
    }
}
