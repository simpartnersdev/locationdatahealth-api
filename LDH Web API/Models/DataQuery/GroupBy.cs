﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models.DataQuery
{
    public class GroupBy
    {
        public string Dimension { get; set; }
        public string ColumnName { get; set; }
        public SortOrder Order { get; set; }
    }
}