﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models.DataQuery
{
    public class ProfileScoreRequest
    {
        private ProfileScoreOrderBy _orderBy;

        public List<Attribute> DimensionFilters { get; set; }
        public PaginationInformation Pagination { get; set; }
        public bool ProfilesWithDifferencesOnly { get; set; }
        public ProfileScoreOrderBy OrderBy
        {
            get
            {
                //set default value for order by score
                if (_orderBy == null)
                {
                    _orderBy = new ProfileScoreOrderBy();
                    _orderBy.Column = ProfileScoreOrderColumn.Score;
                    _orderBy.Ascending = true;
                }
                return _orderBy;
            }
            set
            {
                _orderBy = value;
            }
        }
    }
}