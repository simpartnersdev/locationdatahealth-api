﻿using LDH_Web_API.DAL;

namespace LDH_Web_API.Models.DataQuery
{
    public interface IAPIResponse
    {
        Status Status { get; set; }
    }
}
