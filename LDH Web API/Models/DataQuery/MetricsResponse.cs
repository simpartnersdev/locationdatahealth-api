﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LDH_Web_API.Models.DataQuery
{
    /// <summary>
    /// Contains the response of the query, indicating the totals, and including
    /// details if a Group By instruction is used in the query.
    /// </summary>
    public class MetricsResponse : IAPIResponse
    {
        public Metrics Summary { get; set; }
        public List<Item> Items { get; set; }
        public Status Status { get; set; }
    }
}
