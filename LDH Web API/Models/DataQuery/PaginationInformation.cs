﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models.DataQuery
{
    /// <summary>
    /// Contains the data for pagination of the response, indicating starting point and size of the page.
    /// </summary>
    public class PaginationInformation
    {
        private int _size;
        private int _from;

        /// <summary>
        /// Starting point for the pagination, which represents the index of the record
        /// in the search result
        /// </summary>
        public int From
        {
            get
            {
                return _from;
            }
            set
            {
                _from = value < 1 ? 1 : value;
            }
        }
        /// <summary>
        /// Size of the page (how many records). Min value = 1.
        /// </summary>
        public int Size
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value < 1 ? 1 : value;
            }
        }
    }
}