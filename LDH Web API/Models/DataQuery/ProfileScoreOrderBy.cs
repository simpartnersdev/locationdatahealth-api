﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models.DataQuery
{
    public class ProfileScoreOrderBy
    {
        public ProfileScoreOrderColumn Column { get; set; }
        public bool Ascending { get; set; }
    }

    public enum ProfileScoreOrderColumn
    {
        Score,
        ScoreDelta,
        None
    }
}