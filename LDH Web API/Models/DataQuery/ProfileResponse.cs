﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models.DataQuery
{
    public class ProfileResponse : IAPIResponse
    {
        public List<Profile> Profiles { get; set; }
        public Status Status { get; set; }
    }
}