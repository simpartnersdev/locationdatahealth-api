﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models.DataQuery
{
    public class ListingResponse : IAPIResponse
    {
        public Metrics Summary { get; set; }
        public PaginationInformation Pagination { get; set; }
        public List<Profile> Profiles { get; set; }
        public Status Status { get; set; }
    }
}