﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models.DataQuery
{
    public class ListingOrderBy
    {
        public ListingOrderColumn Column { get; set; }
        public bool Ascending { get; set; }
    }

    public enum ListingOrderColumn
    {
        ListingScore,
        ListingScoreDelta,
        IssueCount,
        TotalListingCount,
        ActualListingCount,
        ProfileScore,
        ProfileScoreDelta
    }
}