﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models.DataQuery
{
    public class ProfileRequest
    {
        public List<string> Columns { get; set; }
        public List<Attribute> DimensionFilters { get; set; }
        public List<OrderByColumn> OrderBy { get; set; }
    }
}