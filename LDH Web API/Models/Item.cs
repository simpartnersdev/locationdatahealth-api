﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models
{
    public class Item
    {
        public string Name { get; set; }
        public string ID { get; set; }
        public Metrics GroupTotal { get; set; }
    }
}