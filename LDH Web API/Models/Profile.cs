﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LDH_Web_API.Models
{
    public class Profile
    {
        public int? ResultsetIndex { get; set; }
        public long ID { get; set; }
        public NAPInfo NAPInfo { get; set; }
        public Metrics Metrics { get; set; }
        public List<Listing> Listings { get; set; }
        public GeoLocation GeoLocation { get; set; }
        public string DirectoryName { get; set; }
        public long? DirectoryID { get; set; }
        public Polygon Polygon { get; set; }
    }
}