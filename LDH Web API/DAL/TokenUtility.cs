﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace LDH_Web_API.DAL.Token
{
    public class Utility
    {

        public static StatusToken GetToken(string WebAPI_URL, string UserName, string Password)
        {
           
            string jsonBody = String.Format("{{UserName:\"{0}\", Password:\"{1}\"}}", UserName, Password);
            
            IRestResponse Response;
            RestRequest Request = null;
            var Client = new RestClient(WebAPI_URL);

            Request = new RestRequest("/api/token", Method.POST);
            Request.AddParameter("Application/Json", jsonBody, ParameterType.RequestBody);

            Request.Timeout = 10 * 1000;

            Response = Client.Execute(Request);
            if (Response.StatusCode.ToString() != "OK")
            {
                throw new Exception(Response.StatusCode.ToString());
            }

            return JsonConvert.DeserializeObject<StatusToken>(Response.Content);
        
        }

        public static TokenResponse DoValidation(string WebAPI_URL, string Token)
        {
            IRestResponse Response;
            RestRequest Request = null;
            var Client = new RestClient(WebAPI_URL);

            Request = new RestRequest("/api/token/validation", Method.GET);
            Request.AddHeader("token", Token);

            Request.Timeout = 10 * 1000;

            Response = Client.Execute(Request);
            if (Response.StatusCode.ToString() != "OK")
            {
                throw new Exception(Response.StatusCode.ToString());
            }

            return JsonConvert.DeserializeObject<TokenResponse>(Response.Content);
        }
    }

    public class TokenResponse
    {
        public SessionJson SessionJson { get; set; }
        public ResponseStatus Status { get; set; }

        public TokenResponse()
        {
            this.Status = new ResponseStatus();
        }
    }

    public class TokenRequest
    {
        public string Token { get; set; }
        public int UserID { get; set; }
        public int ClientID { get; set; }

        public TokenRequest(string Token, int ClientID, int UserID)
        {
            this.Token = Token;
            this.ClientID = ClientID;
            this.UserID = UserID;
        }
    }

    public class SessionJson
    {
        public int UserID { get; set; }
        public int DirectoryID { get; set; }

        public SessionJson() { }
    }

    public class ResponseStatus
    {
        public bool Success { get; set; }
        public string Error { get; set; }
    }

    public class StatusToken
    {
        public bool Success { get; set; }
        public string Token { get; set; }
        public string Error { get; set; }
    }
}