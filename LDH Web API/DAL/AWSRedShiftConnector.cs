﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using LDH_Web_API.Models;
using LDH_Web_API.Models.DataQuery;

namespace LDH_Web_API.DAL
{
    public class AWSRedShiftConnector
    {
        private static string Schema = ConfigurationManager.AppSettings["schema"] != string.Empty ? ConfigurationManager.AppSettings["schema"] + "." : string.Empty;
        
        /*************************/
        /**   YE BE WARNED...   **/
        /**                     **/
        /**   HERE BE DRAGONS   **/
        /*************************/
        
        #region Listings
        /*************************/
        /**  ABANDON ALL HOPE   **/
        /**                     **/
        /**  YE WHO ENTER HERE  **/
        /*************************/
        public ListingResponse GetListings(ListingRequest req, int DirectoryID)
        {
            var dtListings = new DataTable();
            var dtProfilesSummary = new DataTable();

            try
            {
                OdbcDataAdapter daListings = new OdbcDataAdapter(GetListingsQuery(req, DirectoryID), Utils.GetRedshiftConnectionString());
                //OdbcDataReader drListings = 
                OdbcDataAdapter daProfilesSummary = new OdbcDataAdapter(GetProfilesSummaryQuery(req, DirectoryID), Utils.GetRedshiftConnectionString());

                //sync code for testing
                //daListings.Fill(dtListings);
                //daProfilesSummary.Fill(dtProfilesSummary);

                //async code for prod
                List<Task> queries = new List<Task>()
                {
                    Task.Factory.StartNew(() => { daListings.Fill(dtListings); }),
                    Task.Factory.StartNew(() => { daProfilesSummary.Fill(dtProfilesSummary); })
                };

                Task.WaitAll(queries.ToArray());
            }
            catch (Exception ex)
            {
                throw;
            }

            var resp = MapToListings(dtListings, dtProfilesSummary, req);

            resp.Status = new Status(true);

            return resp;
        }

        private string GetListingsQuery(ListingRequest req, int DirectoryID)
        {
            var sbQuery = new StringBuilder();
            //this is to keep track of which dimensions have already been joined
            var lstJoinedDimensions = new List<string>();

            sbQuery.AppendLine("WITH listings AS (");
            sbQuery.AppendLine("  SELECT");

            #region Row Number
            //WARNING!! PAGING ORDER MUST PREVAIL!
            //ORDER BY FOR DENSE_RANK() SHOULD BE THE SAME AS THE FINAL SELECT DOWN BELOW, MINUS ONE LEVEL
            // i.e.:
            //  if select down below uses "order by date, score, id, domain"
            //  then order by up here should be "order by date, score, id" (minus domain)
            var OrderColumn = string.Empty;
            switch (req.OrderBy.Column)
            {
                case ListingOrderColumn.IssueCount:
                    OrderColumn = string.Format("prs_profile_score.prs_issue_count {0}, lis_listing_score.lis_issue_count {0}", req.OrderBy.Ascending ? "ASC" : "DESC");
                    break;
                case ListingOrderColumn.ListingScore:
                    if (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain"))
                    {
                        OrderColumn = string.Format("prs_profile_score.prs_score {0}, lis_listing_score.lis_correctness {0}", req.OrderBy.Ascending ? "ASC" : "DESC"); 
                    }
                    else
                    {
                        OrderColumn = string.Format("prs_profile_score.prs_score {0}, lis_listing_score.lis_profile_domain_score {0}", req.OrderBy.Ascending ? "ASC" : "DESC");
                    }
                    break;
                case ListingOrderColumn.ListingScoreDelta:
                    if (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain"))
                    {
                        OrderColumn = string.Format("lis_listing_score.lis_correctness_delta {0} NULLS LAST", req.OrderBy.Ascending ? "ASC" : "DESC");
                    }
                    else
                    {
                        OrderColumn = string.Format("prs_profile_score.prs_score_delta {0} NULLS LAST, lis_listing_score.lis_profile_domain_score_delta {0}", req.OrderBy.Ascending ? "ASC" : "DESC");
                    }
                    break;
                case ListingOrderColumn.TotalListingCount:
                    OrderColumn = string.Format("prs_profile_score.prs_total_listing {0}", req.OrderBy.Ascending ? "ASC" : "DESC");
                    break;
                case ListingOrderColumn.ActualListingCount:
                    OrderColumn = string.Format("prs_profile_score.prs_actual_listing_count {0}", req.OrderBy.Ascending ? "ASC" : "DESC");
                    break;
                case ListingOrderColumn.ProfileScore:
                default:
                    if (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain"))
                    {
                        OrderColumn = string.Format("lis_listing_score.lis_correctness {0}", req.OrderBy.Ascending ? "ASC" : "DESC");
                    }
                    else
                    {
                        OrderColumn = string.Format("prs_profile_score.prs_score {0}", req.OrderBy.Ascending ? "ASC" : "DESC");
                    }
                    break;
                case ListingOrderColumn.ProfileScoreDelta:
                    if (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain"))
                    {
                        OrderColumn = string.Format("lis_listing_score.lis_correctness_delta {0} NULLS LAST", req.OrderBy.Ascending ? "ASC" : "DESC");
                    }
                    else
                    {
                        OrderColumn = string.Format("prs_profile_score.prs_score_delta {0} NULLS LAST", req.OrderBy.Ascending ? "ASC" : "DESC");
                    }
                    break;
            }
            sbQuery.AppendLine(string.Format("    DENSE_RANK() OVER(ORDER BY lis_listing_score.lis_tim_date, {0}, pro_profile.pro_id) AS row_nbr,", OrderColumn));
            #endregion

            #region Alternate HealthScore
            //if dom_domain is used in the filters, it skews the profile_score pre-calculation, so it needs to be re-calculated on the fly
            if (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain"))
            {
                sbQuery.AppendLine("    --TOTALS");
                sbQuery.AppendLine("    lis_listing_score.lis_correctness TotalHealthScoreByDomain,");
                sbQuery.AppendLine("    lis_listing_score.lis_correctness_delta TotalHealthScoreDeltaByDomain,");
            }
            #endregion

            #region Profile Info
            sbQuery.AppendLine("    --PROFILE SCORING");
            if (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain"))
            {
                sbQuery.AppendLine("    lis_listing_score.lis_correctness prs_score,");
                sbQuery.AppendLine("    lis_listing_score.lis_correctness_delta prs_score_delta,");
                sbQuery.AppendLine("    lis_listing_score.lis_listing_exists prs_total_listing_count,");
                sbQuery.AppendLine("    lis_listing_score.lis_listing_exists prs_actual_listing_count,");
                sbQuery.AppendLine("    lis_listing_score.lis_issue_count prs_issue_count,");
            }
            else
            {
                sbQuery.AppendLine("    prs_profile_score.prs_score,");
                sbQuery.AppendLine("    prs_profile_score.prs_score_delta,");
                sbQuery.AppendLine("    prs_profile_score.prs_total_listing_count,");
                sbQuery.AppendLine("    prs_profile_score.prs_actual_listing_count,");
                sbQuery.AppendLine("    prs_profile_score.prs_issue_count,");
            }
            sbQuery.AppendLine("    --PROFILE DATA");
            sbQuery.AppendLine("    pro_profile.pro_id,");
            sbQuery.AppendLine("    pro_profile.pro_name,");
            sbQuery.AppendLine("    pro_profile.pro_address,");
            sbQuery.AppendLine("    pro_profile.pro_city,");
            sbQuery.AppendLine("    pro_profile.pro_state,");
            sbQuery.AppendLine("    pro_profile.pro_zip_code,");
            sbQuery.AppendLine("    pro_profile.pro_phone,");
            sbQuery.AppendLine("    pro_profile.pro_website,");
            sbQuery.AppendLine("    pro_profile.pro_directory_id,"); //TODO: not needed in select?
            sbQuery.AppendLine("    pro_profile.pro_latitude,");
            sbQuery.AppendLine("    pro_profile.pro_longitude,");
            #endregion

            #region Listings Info
            sbQuery.AppendLine("    --LISTING DATA");
            //dom_domain.dom_name could potentially be replaced with lis_listing_score.lis_dom_id, but the join must happen anyway (order by dom_weight)
            sbQuery.AppendLine("    dom_domain.dom_name,"); 
            sbQuery.AppendLine("    lis_listing_score.lis_issue_count,");
            sbQuery.AppendLine("    lis_listing_score.lis_no_match_fields,");
            sbQuery.AppendLine("    lis_listing_score.lis_correctness,");
            sbQuery.AppendLine("    lis_listing_score.lis_correctness_delta,");
            sbQuery.AppendLine("    lis_listing_score.lis_tim_date,"); //TODO: not needed in select?
            sbQuery.AppendLine("    --LISTING DETAILS");
            sbQuery.AppendLine("    lsd_listing_score_detail.lsd_domain_name,");
            sbQuery.AppendLine("    lsd_listing_score_detail.lsd_domain_address,");
            sbQuery.AppendLine("    lsd_listing_score_detail.lsd_domain_city,");
            sbQuery.AppendLine("    lsd_listing_score_detail.lsd_domain_state,");
            sbQuery.AppendLine("    lsd_listing_score_detail.lsd_domain_zip_code,");
            sbQuery.AppendLine("    lsd_listing_score_detail.lsd_domain_phone,");
            sbQuery.AppendLine("    lsd_listing_score_detail.lsd_domain_website,");
            sbQuery.AppendLine("    lsd_listing_score_detail.lsd_full_date,");
            sbQuery.AppendLine("    lsd_listing_score_detail.lsd_link");
            #endregion

            #region From
            sbQuery.AppendLine("  FROM");
            sbQuery.AppendLine(string.Format("    {0}lis_listing_score", Schema));
            sbQuery.AppendLine(string.Format("    INNER JOIN {0}pro_profile ON lis_listing_score.lis_pro_id = pro_profile.pro_id", Schema));
            sbQuery.AppendLine(string.Format("    INNER JOIN {0}dom_domain ON lis_listing_score.lis_dom_id = dom_domain.dom_id", Schema));
            sbQuery.AppendLine(string.Format("    INNER JOIN {0}lsd_listing_score_detail ON lis_listing_score.lis_id = lsd_listing_score_detail.lsd_lis_id", Schema));
            sbQuery.AppendLine(string.Format("    INNER JOIN {0}prs_profile_score ON lis_listing_score.lis_pro_id = prs_profile_score.prs_pro_id", Schema));
            sbQuery.AppendLine("                                    AND lis_listing_score.lis_tim_date = prs_profile_score.prs_tim_date");

            //keep track of joined dimensions
            lstJoinedDimensions.Add("pro_profile");
            lstJoinedDimensions.Add("dom_domain");

            //EXTRA JOINS FROM REQUEST QUERY HERE
            if (req.DimensionFilters != null)
            {
                //first add required dimensions, if not already added
                foreach (var filter in req.DimensionFilters)
                {
                    if (!lstJoinedDimensions.Contains(filter.Dimension.ToLower()))
                    {
                        switch (filter.Dimension.ToLower())
                        {
                            case "dom_domain":
                                //do nothing, already joined above
                                break;

                            case "mic_micro_market":
                                //pol_polygon may have been already added, check first!
                                if (!lstJoinedDimensions.Contains("pol_polygon"))
                                {
                                    sbQuery.AppendLine(string.Format("    LEFT JOIN {0}pol_polygon ON pol_polygon.pol_id = lis_listing_score.lis_pol_id", Schema));
                                    lstJoinedDimensions.Add("pol_polygon");
                                }
                                sbQuery.AppendLine(string.Format("    LEFT JOIN {0}mic_micro_market ON mic_micro_market.mic_id = pol_polygon.pol_mic_id", Schema));
                                break;

                            case "pro_profile":
                                //do nothing, already joined above
                                break;

                            case "tim_time":
                                sbQuery.AppendLine(string.Format("    INNER JOIN {0}tim_time ON tim_time.tim_date = lis_listing_score.lis_tim_date", Schema));
                                break;

                            case "pol_polygon":
                                sbQuery.AppendLine(string.Format("    LEFT JOIN {0}pol_polygon ON pol_polygon.pol_id = lis_listing_score.lis_pol_id", Schema));
                                break;

                            case "lis_listing_score":
                            case "lsd_listing_score_detail":
                            case "prs_profile_score":
                            case "pog_polygon_geometry":
                                throw new WrongDimensionException(filter.Dimension, filter.ColumnName, string.Format("Not a dimension: [{0}.{1}].", filter.Dimension, filter.ColumnName));

                            default:
                                throw new WrongDimensionException(filter.Dimension, filter.ColumnName, string.Format("Unknown dimension reference: [{0}.{1}].", filter.Dimension, filter.ColumnName));
                        }
                        lstJoinedDimensions.Add(filter.Dimension.ToLower());
                    }
                }

            }
            #endregion

            #region Where
            sbQuery.AppendLine("  WHERE");
            sbQuery.AppendLine(string.Format("      pro_profile.pro_directory_id = {0}", DirectoryID));

            if (req.DimensionFilters != null)
            {
                foreach (var filter in req.DimensionFilters)
                {
                    sbQuery.AppendLine(string.Format("	    AND {0}.{1} IN ({2})", filter.Dimension, filter.ColumnName, filter.Values.ToCsv()));
                }

            }
            #endregion

            #region Order By
            //WARNING!! PAGING ORDER MUST PREVAIL!
            //DON'T ALTER THE ORDER BY! (Unless you know EXACTLY what you're doing)
            //Must guarantee same order every time, so paging works
            sbQuery.AppendLine("  ORDER BY");
            sbQuery.AppendLine("      lis_listing_score.lis_tim_date,");

            switch (req.OrderBy.Column)
            {
                case ListingOrderColumn.IssueCount:
                    sbQuery.AppendLine(string.Format("   prs_profile_score.prs_issue_count {0}, lis_listing_score.lis_issue_count {0},", req.OrderBy.Ascending ? "ASC" : "DESC"));
                    break;
                case ListingOrderColumn.ListingScore:
                    if (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain"))
                    {
                        sbQuery.AppendLine(string.Format("   prs_profile_score.prs_score {0}, lis_listing_score.lis_correctness {0},", req.OrderBy.Ascending ? "ASC" : "DESC"));
                    }
                    else
                    {
                        sbQuery.AppendLine(string.Format("   prs_profile_score.prs_score {0}, lis_listing_score.lis_profile_domain_score {0},", req.OrderBy.Ascending ? "ASC" : "DESC"));
                    }
                    break;
                case ListingOrderColumn.ListingScoreDelta:
                    if (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain"))
                    {
                        sbQuery.AppendLine(string.Format("   prs_profile_score.prs_score_delta {0} NULLS LAST, lis_listing_score.lis_correctness_delta {0},", req.OrderBy.Ascending ? "ASC" : "DESC"));
                    }
                    else
                    {
                        sbQuery.AppendLine(string.Format("   prs_profile_score.prs_score_delta {0} NULLS LAST, lis_listing_score.lis_profile_domain_score_delta {0},", req.OrderBy.Ascending ? "ASC" : "DESC"));
                    }
                    break;
                case ListingOrderColumn.TotalListingCount:
                    sbQuery.AppendLine(string.Format("   prs_profile_score.prs_actual_listing_count {0},", req.OrderBy.Ascending ? "ASC" : "DESC"));
                    break;
                case ListingOrderColumn.ActualListingCount:
                    sbQuery.AppendLine(string.Format("   prs_profile_score.prs_total_listing {0},", req.OrderBy.Ascending ? "ASC" : "DESC"));
                    break;
                case ListingOrderColumn.ProfileScore:
                default:
                    if (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain"))
                    {
                        sbQuery.AppendLine(string.Format("    lis_listing_score.lis_correctness {0},", req.OrderBy.Ascending ? "ASC" : "DESC"));
                    }
                    else
                    {
                        sbQuery.AppendLine(string.Format("    prs_profile_score.prs_score {0},", req.OrderBy.Ascending ? "ASC" : "DESC"));
                    }
                    break;
                case ListingOrderColumn.ProfileScoreDelta:
                    if (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain"))
                    {
                        sbQuery.AppendLine(string.Format("    lis_listing_score.lis_correctness_delta {0} NULLS LAST,", req.OrderBy.Ascending ? "ASC" : "DESC"));
                    }
                    else
                    {
                        sbQuery.AppendLine(string.Format("    prs_profile_score.prs_score_delta {0} NULLS LAST,", req.OrderBy.Ascending ? "ASC" : "DESC"));
                    }
                    break;
            }

            sbQuery.AppendLine("      pro_profile.pro_id,");
            sbQuery.AppendLine("      dom_domain.dom_weight DESC"); //TODO: should this be moved to lsd_score_details to avoid a join? (dom_domain is not a big table anyways)
            sbQuery.AppendLine("  )");
            #endregion

            #region External SELECT
            sbQuery.AppendLine("SELECT ");
            sbQuery.AppendLine("  *");
            sbQuery.AppendLine("FROM ");
            sbQuery.AppendLine("  listings");
            sbQuery.AppendLine("WHERE ");
            //this is just to make it easier to add the "AND" stuff later
            sbQuery.AppendLine("  1 = 1");

            if (req.ListingsWithDifferencesOnly)
            {
                sbQuery.AppendLine("  AND lis_issue_count > 0");
            }

            //paging control
            if (req.Pagination != null && req.Pagination.Size > 0)
            {
                sbQuery.AppendLine("    AND row_nbr >= " + req.Pagination.From.ToString());
                sbQuery.AppendLine("    AND row_nbr < " + (req.Pagination.From + req.Pagination.Size).ToString());
            }
            #endregion

            //gotta close the instruction
            sbQuery.Append(";");

            return sbQuery.ToString();
        }

        private string GetProfilesSummaryQuery(ListingRequest req, int DirectoryID)
        {
            var sbQuery = new StringBuilder();
            
            sbQuery.AppendLine("SELECT");

            //if domain is being used for filtering, things get crazy, 
            //must switch to score by listings, can't use pre-calculated profile scores
            if (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain"))
            {
                sbQuery.AppendLine("  AVG(lis_listing_score.lis_correctness) AS TotalHealthScore,");
                sbQuery.AppendLine("  AVG(lis_listing_score.lis_correctness_delta) AS TotalHealthScoreDelta,");
                sbQuery.AppendLine("  COUNT(DISTINCT pro_profile.pro_id) AS TotalProfileCount,");
                sbQuery.AppendLine("  COUNT(1) AS TotalListingCount,");
                sbQuery.AppendLine("  SUM(CAST(ISNULL(lis_listing_score.lis_listing_exists, FALSE) AS INT)) ActualListingsCount,");
                sbQuery.AppendLine("  SUM(lis_listing_score.lis_issue_count) AS TotalIssueCount");
            }
            else
            {
                sbQuery.AppendLine("  AVG(prs_profile_score.prs_score) AS TotalHealthScore,");
                sbQuery.AppendLine("  AVG(prs_profile_score.prs_score_delta) AS TotalHealthScoreDelta,");
                sbQuery.AppendLine("  COUNT(DISTINCT pro_profile.pro_id) AS TotalProfileCount,");
                sbQuery.AppendLine("  SUM(prs_profile_score.prs_total_listing_count) AS TotalListingCount,");
                sbQuery.AppendLine("  SUM(prs_profile_score.prs_actual_listing_count) AS ActualListingsCount,");
                sbQuery.AppendLine("  SUM(prs_profile_score.prs_issue_count) AS TotalIssueCount");
            }

            sbQuery.AppendLine("FROM");
            sbQuery.AppendLine(string.Format("  {0}prs_profile_score", Schema));
            sbQuery.AppendLine(string.Format("  INNER JOIN {0}pro_profile ON prs_profile_score.prs_pro_id = pro_profile.pro_id", Schema));

            //this is to keep track of which dimensions have already been joined
            var lstJoinedDimensions = new List<string>();
            lstJoinedDimensions.Add("pro_profile");

            //EXTRA JOINS FROM REQUEST QUERY HERE
            if (req.DimensionFilters != null && req.DimensionFilters.Count > 0)
            {
                //first add required dimensions, if not already added
                foreach (var filter in req.DimensionFilters)
                {
                    if (!lstJoinedDimensions.Contains(filter.Dimension.ToLower()))
                    {
                        switch (filter.Dimension.ToLower())
                        {
                            case "dom_domain":
                                sbQuery.AppendLine(string.Format("  INNER JOIN {0}lis_listing_score ON lis_listing_score.lis_pro_id = prs_profile_score.prs_pro_id", Schema));
                                sbQuery.AppendLine("                                  AND lis_listing_score.lis_tim_date = prs_profile_score.prs_tim_date");
                                sbQuery.AppendLine(string.Format("  INNER JOIN {0}dom_domain ON lis_listing_score.lis_dom_id = dom_domain.dom_id", Schema));  
                                break;

                            case "mic_micro_market":
                                //pol_polygon may have been already added, check first!
                                if (!lstJoinedDimensions.Contains("pol_polygon"))
                                {
                                    sbQuery.AppendLine(string.Format("  LEFT JOIN {0}pol_polygon ON pol_polygon.pol_id = prs_profile_score.prs_pol_id", Schema));
                                    lstJoinedDimensions.Add("pol_polygon");
                                }
                                sbQuery.AppendLine(string.Format("  LEFT JOIN {0}mic_micro_market ON mic_micro_market.mic_id = pol_polygon.pol_mic_id", Schema));
                                break;

                            case "pro_profile":
                                //do nothing, already joined above
                                break;

                            case "tim_time":
                                sbQuery.AppendLine(string.Format("  INNER JOIN {0}tim_time ON tim_time.tim_date = prs_profile_score.prs_tim_date", Schema));
                                break;

                            case "pol_polygon":
                                sbQuery.AppendLine(string.Format("  LEFT JOIN {0}pol_polygon ON pol_polygon.pol_id = prs_profile_score.prs_pol_id", Schema));
                                break;

                            case "lis_listing_score":
                            case "lsd_listing_score_detail":
                            case "prs_profile_score":
                            case "pog_polygon_geometry":
                                throw new WrongDimensionException(filter.Dimension, filter.ColumnName, string.Format("Not a dimension: [{0}.{1}].", filter.Dimension, filter.ColumnName));

                            default:
                                throw new WrongDimensionException(filter.Dimension, filter.ColumnName, string.Format("Unknown dimension reference: [{0}.{1}].", filter.Dimension, filter.ColumnName));
                        }
                        lstJoinedDimensions.Add(filter.Dimension.ToLower());
                    }
                }

                sbQuery.AppendLine("WHERE");
                sbQuery.AppendLine(string.Format("  pro_profile.pro_directory_id = {0}", DirectoryID));

                foreach (var filter in req.DimensionFilters)
                {
                    sbQuery.AppendLine(string.Format("    AND {0}.{1} IN ({2})", filter.Dimension, filter.ColumnName, filter.Values.ToCsv()));
                }
            }

            //gotta close the instruction
            sbQuery.Append(";");

            return sbQuery.ToString();
        }

        private ListingResponse MapToListings(DataTable Listings, DataTable ProfilesSummary, ListingRequest req)
        {
            var resp = new ListingResponse();

            resp.Pagination = req.Pagination;

            if (ProfilesSummary.Rows.Count > 0 && Listings.Rows.Count > 0)
            {
                resp.Summary = new Metrics();

                #region Summary
                resp.Summary.HealthScore = Convert.ToDecimal(ProfilesSummary.Rows[0]["TotalHealthScore"]); //AVG already calculated on SQL
                resp.Summary.HealthScoreDelta = ProfilesSummary.Rows[0]["TotalHealthScoreDelta"] != DBNull.Value ? (decimal?)Convert.ToDecimal(ProfilesSummary.Rows[0]["TotalHealthScoreDelta"]) : null;
                resp.Summary.IssueCount = Convert.ToInt32(ProfilesSummary.Rows[0]["TotalIssueCount"]);
                resp.Summary.ListingCount = Convert.ToInt32(ProfilesSummary.Rows[0]["TotalListingCount"]);
                resp.Summary.ActualListingsFoundCount = Convert.ToInt32(ProfilesSummary.Rows[0]["ActualListingsCount"]);
                resp.Summary.ProfileCount = Convert.ToInt32(ProfilesSummary.Rows[0]["TotalProfileCount"]);
                #endregion
            }

            if (Listings.Rows.Count > 0)
            {
                resp.Profiles = new List<Profile>();
                var _lobj = new object();

                for (int i = 0; i < Listings.Rows.Count; i++)
                {
                    #region Profile
                    //create profile if not created already
                    if (!resp.Profiles.Exists(x => x.ID == Convert.ToInt64(Listings.Rows[i]["pro_id"])))
                    {
                        resp.Profiles.Add(new Profile()
                        {
                            ResultsetIndex = Convert.ToInt32(Listings.Rows[i]["row_nbr"]),
                            ID = Convert.ToInt64(Listings.Rows[i]["pro_id"]),
                            GeoLocation = new GeoLocation()
                            {
                                //need to force Culture on lat/lon so convert.todecimal won't fail when running in different regional settings
                                Latitude = (decimal?)Convert.ToDecimal(Listings.Rows[i]["pro_latitude"], new CultureInfo("en-US")),
                                Longitude = (decimal?)Convert.ToDecimal(Listings.Rows[i]["pro_longitude"], new CultureInfo("en-US"))
                            },
                            NAPInfo = new NAPInfo()
                            {
                                Name = Listings.Rows[i]["pro_name"].ToString(),
                                Address = new Address()
                                {
                                    Address1 = Listings.Rows[i]["pro_address"].ToString(),
                                    City = Listings.Rows[i]["pro_city"].ToString(),
                                    State = Listings.Rows[i]["pro_state"].ToString(),
                                    ZipCode = Listings.Rows[i]["pro_zip_code"].ToString()
                                },
                                Phone = Listings.Rows[i]["pro_phone"].ToString(),
                                WebSite = Listings.Rows[i]["pro_website"].ToString()
                            },
                            Metrics = new Metrics()
                            {
                                HealthScore = Convert.ToDecimal(Listings.Rows[i]["prs_score"]),
                                HealthScoreDelta = Listings.Rows[i]["prs_score_delta"] != DBNull.Value ? (decimal?)Convert.ToDecimal(Listings.Rows[i]["prs_score_delta"]) : null,
                                ListingCount = Convert.ToInt32(Listings.Rows[i]["prs_total_listing_count"]),
                                ActualListingsFoundCount = Convert.ToInt32(Listings.Rows[i]["prs_actual_listing_count"]),
                                IssueCount = Convert.ToInt32(Listings.Rows[i]["prs_issue_count"])
                            },
                            Listings = new List<Listing>()
                        });

                    }
                    #endregion

                    #region Listing
                    //add listing to profile
                    resp.Profiles.Find(x => x.ID == Convert.ToInt64(Listings.Rows[i]["pro_id"])).Listings.Add(new Listing()
                    {
                        Domain = Listings.Rows[i]["dom_name"].ToString(),
                        NAPInfo = new NAPInfo()
                        {
                            Name = Listings.Rows[i]["lsd_domain_name"].ToString(),
                            Address = new Address()
                            {
                                Address1 = Listings.Rows[i]["lsd_domain_address"].ToString(),
                                City = Listings.Rows[i]["lsd_domain_city"].ToString(),
                                State = Listings.Rows[i]["lsd_domain_state"].ToString(),
                                ZipCode = Listings.Rows[i]["lsd_domain_zip_code"].ToString()
                            },
                            Phone = Listings.Rows[i]["lsd_domain_phone"].ToString(),
                            WebSite = Listings.Rows[i]["lsd_domain_website"].ToString()
                        },
                        Metrics = new Metrics()
                        {
                            HealthScore = Convert.ToDecimal(Listings.Rows[i]["lis_correctness"]),
                            HealthScoreDelta = Listings.Rows[i]["lis_correctness_delta"] != DBNull.Value ? (decimal?)Convert.ToDecimal(Listings.Rows[i]["lis_correctness_delta"]) : null,
                            IssueCount = Convert.ToInt32(Listings.Rows[i]["lis_issue_count"])
                        },
                        NoMatchFields = new List<string>(Listings.Rows[i]["lis_no_match_fields"].ToString().Split(',')),
                        ProcessDate = Convert.ToDateTime(Listings.Rows[i]["lsd_full_date"]),
                        URL = Listings.Rows[i]["lsd_link"].ToString()
                    });
                    #endregion
                }
            }

            return resp;
        }

        private ListingResponse MapToListings(System.Data.SqlClient.SqlDataReader Listings, DataTable ProfilesSummary, ListingRequest req)
        {
            var resp = new ListingResponse();

            resp.Pagination = req.Pagination;

            if (ProfilesSummary.Rows.Count > 0 && Listings.HasRows)
            {
                resp.Summary = new Metrics();

                #region Summary
                //if domain is being used for filtering, things get crazy, 
                //must switch to score correctness
                if (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain"))
                {
                    resp.Summary.HealthScore = Convert.ToDecimal(Listings["TotalHealthScoreByDomain"]);
                    resp.Summary.HealthScoreDelta = Listings["TotalHealthScoreDeltaByDomain"] != DBNull.Value ? (decimal?)Convert.ToDecimal(Listings["TotalHealthScoreDeltaByDomain"]) : null;
                }
                else
                {
                    resp.Summary.HealthScore = Convert.ToDecimal(ProfilesSummary.Rows[0]["TotalHealthScore"]); //AVG already calculated on SQL
                    resp.Summary.HealthScoreDelta = ProfilesSummary.Rows[0]["TotalHealthScoreDelta"] != DBNull.Value ? (decimal?)Convert.ToDecimal(ProfilesSummary.Rows[0]["TotalHealthScoreDelta"]) : null;
                }
                resp.Summary.IssueCount = Convert.ToInt32(ProfilesSummary.Rows[0]["TotalIssueCount"]);
                resp.Summary.ListingCount = Convert.ToInt32(ProfilesSummary.Rows[0]["TotalListingCount"]);
                resp.Summary.ActualListingsFoundCount = Convert.ToInt32(ProfilesSummary.Rows[0]["ActualListingsCount"]);
                resp.Summary.ProfileCount = Convert.ToInt32(ProfilesSummary.Rows[0]["TotalProfileCount"]);
                #endregion

                resp.Profiles = new List<Profile>();

                while (Listings.Read())
                {
                    #region Profile
                    //create profile if not created already
                    if (!resp.Profiles.Exists(x => x.ID == Convert.ToInt64(Listings["pro_id"])))
                    {
                        resp.Profiles.Add(new Profile()
                        {
                            ResultsetIndex = Convert.ToInt32(Listings["row_nbr"]),
                            ID = Convert.ToInt64(Listings["pro_id"]),
                            GeoLocation = new GeoLocation()
                            {
                                //need to force Culture on lat/lon so convert.todecimal won't fail when running in different regional settings
                                Latitude = (decimal?)Convert.ToDecimal(Listings["pro_latitude"], new CultureInfo("en-US")),
                                Longitude = (decimal?)Convert.ToDecimal(Listings["pro_longitude"], new CultureInfo("en-US"))
                            },
                            NAPInfo = new NAPInfo()
                            {
                                Name = Listings["pro_name"].ToString(),
                                Address = new Address()
                                {
                                    Address1 = Listings["pro_address"].ToString(),
                                    City = Listings["pro_city"].ToString(),
                                    State = Listings["pro_state"].ToString(),
                                    ZipCode = Listings["pro_zip_code"].ToString()
                                },
                                Phone = Listings["pro_phone"].ToString(),
                                WebSite = Listings["pro_website"].ToString()
                            },
                            Metrics = new Metrics()
                            {
                                HealthScore = Convert.ToDecimal(Listings["prs_score"]),
                                HealthScoreDelta = Listings["prs_score_delta"] != DBNull.Value ? (decimal?)Convert.ToDecimal(Listings["prs_score_delta"]) : null,
                                ListingCount = Convert.ToInt32(Listings["prs_total_listing_count"]),
                                ActualListingsFoundCount = Convert.ToInt32(Listings["prs_actual_listing_count"]),
                                IssueCount = Convert.ToInt32(Listings["prs_issue_count"])
                            },
                            Listings = new List<Listing>()
                        });
                    }
                    #endregion

                    #region Listing
                    //add listing to profile
                    resp.Profiles.Find(x => x.ID == Convert.ToInt64(Listings["pro_id"])).Listings.Add(new Listing()
                    {
                        Domain = Listings["dom_name"].ToString(),
                        NAPInfo = new NAPInfo()
                        {
                            Name = Listings["lsd_domain_name"].ToString(),
                            Address = new Address()
                            {
                                Address1 = Listings["lsd_domain_address"].ToString(),
                                City = Listings["lsd_domain_city"].ToString(),
                                State = Listings["lsd_domain_state"].ToString(),
                                ZipCode = Listings["lsd_domain_zip_code"].ToString()
                            },
                            Phone = Listings["lsd_domain_phone"].ToString(),
                            WebSite = Listings["lsd_domain_website"].ToString()
                        },
                        Metrics = new Metrics()
                        {
                            HealthScore = Convert.ToDecimal(Listings["lis_correctness"]),
                            HealthScoreDelta = Listings["lis_correctness_delta"] != DBNull.Value ? (decimal?)Convert.ToDecimal(Listings["lis_correctness_delta"]) : null,
                            IssueCount = Convert.ToInt32(Listings["lis_issue_count"])
                        },
                        NoMatchFields = new List<string>(Listings["lis_no_match_fields"].ToString().Split(',')),
                        ProcessDate = Convert.ToDateTime(Listings["lsd_full_date"]),
                        URL = Listings["lsd_link"].ToString()
                    });
                    #endregion
                }
            }

            return resp;
        }
        #endregion

        #region Metrics
        public MetricsResponse GetMetrics(MetricsRequest req, int DirectoryID)
        {
            var conn = new OdbcConnection(Utils.GetRedshiftConnectionString());
            var ds = new DataSet();
            var dt = new DataTable();

            //CHECK FOR SPECIAL CASE
            //GroupBY Month AND NO FILTERS ==> call HistoricalDataByMonth method instead
            if ((req.DimensionFilters == null || (req.DimensionFilters != null && req.DimensionFilters.Count == 0))
                && (req.GroupBy != null && req.GroupBy.Dimension.ToLower() == "tim_time" && (req.GroupBy.ColumnName.ToLower() == "tim_month" || req.GroupBy.ColumnName.ToLower() == "tim_month_name")))
            {
                //in this case, we need to respond based on a specific query to include historical data
                try
                {
                    conn.Open();
                    OdbcDataAdapter da = new OdbcDataAdapter(GetMetricsHistoricalDataByMonth(req, DirectoryID), conn);
                    da.Fill(ds);

                    dt = ds.Tables[0];
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }

                var respHistoricalData = MapToMetricsHistoricalDataByMonth(dt, req);

                respHistoricalData.Status = new Status(true);

                return respHistoricalData;
            }
            else
            {
                //THIS else block is the normal response path
                try
                {
                    conn.Open();
                    OdbcDataAdapter da = new OdbcDataAdapter(GetMetricsQuery(req, DirectoryID), conn);
                    da.Fill(ds);

                    dt = ds.Tables[0];
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }

                var resp = MapToMetrics(dt, req);

                resp.Status = new Status(true);

                return resp;
            }
        }

        private string GetMetricsHistoricalDataByMonth(MetricsRequest req, int DirectoryID)
        {
            //this is a special ad-hoc query specifically built for populating a dataset required by the UI
            //in order to show historical data which includes periods of time prior to current Zycara parsed data
            var sbQuery = new StringBuilder();

            sbQuery.AppendLine("SELECT");
            sbQuery.AppendLine("  T.tim_month_name,");
            sbQuery.AppendLine("  T.tim_month,");
            sbQuery.AppendLine("  AVG(prs_profile_score.prs_score) AS HealthScore,");
            sbQuery.AppendLine("  AVG(prs_profile_score.prs_score_delta) AS HealthScoreDelta,");
            sbQuery.AppendLine("  SUM(prs_profile_score.prs_issue_count) AS IssueCount");
            sbQuery.AppendLine("FROM");
            sbQuery.AppendLine(string.Format("  {0}prs_profile_score", Schema));
            sbQuery.AppendLine(string.Format("  INNER JOIN {0}pro_profile ON prs_profile_score.prs_pro_id = pro_profile.pro_id", Schema));
            sbQuery.AppendLine(string.Format("  INNER JOIN {0}tim_time T ON prs_profile_score.prs_tim_date = T.tim_date", Schema));
            sbQuery.AppendLine("WHERE");
            sbQuery.AppendLine(string.Format("  pro_profile.pro_directory_id = {0}", DirectoryID));
            sbQuery.AppendLine("GROUP BY");
            sbQuery.AppendLine("  T.tim_month_name,");
            sbQuery.AppendLine("  T.tim_month");
            sbQuery.AppendLine("UNION ALL");
            sbQuery.AppendLine("SELECT");
            sbQuery.AppendLine("  T.tim_month_name,");
            sbQuery.AppendLine("  T.tim_month,");
            sbQuery.AppendLine("  AVG(his_history_data.his_health_score) AS HealthScore,");
            sbQuery.AppendLine("  AVG(his_history_data.his_health_score_delta) AS HealthScoreDelta,"); 
            sbQuery.AppendLine("  NULL AS IssueCount");
            sbQuery.AppendLine("FROM");
            sbQuery.AppendLine("  etl.his_history_data");
            sbQuery.AppendLine(string.Format("  INNER JOIN {0}tim_time T ON his_history_data.his_tim_date = T.tim_date", Schema));
            sbQuery.AppendLine("WHERE");
            sbQuery.AppendLine(string.Format("  his_directory_id = {0}", DirectoryID));
            sbQuery.AppendLine("GROUP BY");
            sbQuery.AppendLine("  T.tim_month_name,");
            sbQuery.AppendLine("  T.tim_month");
            if (req.GroupBy != null && req.GroupBy.Order != SortOrder.NONE)
            {
                sbQuery.AppendLine("ORDER BY");
                sbQuery.AppendLine(string.Format("  2 {0}", req.GroupBy.Order.ToString()));
            }
            return sbQuery.ToString();
        }

        private MetricsResponse MapToMetricsHistoricalDataByMonth(DataTable table, MetricsRequest req)
        {
            var resp = new MetricsResponse();

            if (table.Rows.Count > 0)
            {
                if (req.GroupBy != null)
                {
                    resp.Items = new List<Item>();

                    for (int i = 0; i < table.Rows.Count; i++)
                    {
                        resp.Items.Add(new Item()
                        {
                            Name = table.Rows[i]["tim_month"].ToString(),
                            ID = table.Columns.Contains("tim_month") ? table.Rows[i]["tim_month"].ToString() : null,
                            GroupTotal = new Metrics()
                            {
                                HealthScore = Convert.ToDecimal(table.Rows[i]["HealthScore"]),
                                HealthScoreDelta = table.Rows[i]["HealthScoreDelta"] != DBNull.Value ? (decimal?)Convert.ToDecimal(table.Rows[i]["HealthScoreDelta"]) : null,
                                IssueCount = table.Rows[i]["IssueCount"] == DBNull.Value ? null : (int?)Convert.ToInt32(table.Rows[i]["IssueCount"])
                            }
                        });
                    }
                }
            }

            return resp;
        }

        private string GetMetricsQuery(MetricsRequest req, int DirectoryID)
        {
            var sbQuery = new StringBuilder();
            sbQuery.AppendLine("SELECT");

            string strIdField = string.Empty;

            #region Dimension Name/ID
            //only get group name if there's a group by in the request
            if (req.GroupBy != null)
            {
                sbQuery.AppendLine(string.Format("  {0}.{1} Name,", req.GroupBy.Dimension, req.GroupBy.ColumnName));

                switch (req.GroupBy.Dimension.ToLower())
                {
                    case "dom_domain":
                        //dom_domain and dom_id are actually the same (value), so no need to add another column
                        break;
                    case "tim_time":
                        //time dimension works in a unique way, each column represents a specific drill-down
                        //no need to (and can't) put the id in the select as it would alter the logic of the group by
                        break;
                    case "mic_micro_market":
                        strIdField = "mic_id";
                        break;
                    case "pol_polygon":
                        strIdField = "pol_id";
                        break;
                    case "pro_profile":
                        //profile_name vs profile_id works a bit odd:
                        //several clients actually have many profiles using the exact same name
                        //so grouping here comes down to the exact instructions given by the
                        //data in the request. No automatic extra columns added.
                        break;
                }

                if (strIdField != string.Empty)
                {
                    sbQuery.AppendLine(string.Format("  {0}.{1} ID,", req.GroupBy.Dimension, strIdField));
                }
            }
            #endregion

            #region Metrics, check for Domain filter/groupby
            if ((req.GroupBy != null && req.GroupBy.Dimension.ToLower() == "dom_domain"))
            {
                sbQuery.AppendLine("  dom_domain.dom_weight,");
            }

            //if domain is being used for filtering or group by, things get crazy, 
            //must switch to score correctness
            if ((req.GroupBy != null && req.GroupBy.Dimension.ToLower() == "dom_domain")
                || (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain")))
            {
                //ISNULL used to avoid killing the score if a null makes it through the ETL (shouldn't really happen)
                sbQuery.AppendLine("  AVG(ISNULL(lis_listing_score.lis_correctness, 0)) AS HealthScore,");
                sbQuery.AppendLine("  AVG(ISNULL(lis_listing_score.lis_correctness_delta, 0)) AS HealthScoreDelta,");
                sbQuery.AppendLine("  COUNT(DISTINCT pro_profile.pro_id) AS ProfileCount,");
                sbQuery.AppendLine("  COUNT(1) AS ListingCount,");
                sbQuery.AppendLine("  SUM(CAST(ISNULL(lis_listing_score.lis_listing_exists, FALSE) AS INT)) ActualListingCount,");
                sbQuery.AppendLine("  SUM(lis_listing_score.lis_issue_count) AS IssueCount");
            }
            else
            {
                //no need for ISNULL as score is pre-calculated during ETL
                sbQuery.AppendLine("  AVG(prs_profile_score.prs_score) AS HealthScore,");
                sbQuery.AppendLine("  AVG(prs_profile_score.prs_score_delta) AS HealthScoreDelta,");
                sbQuery.AppendLine("  COUNT(DISTINCT pro_profile.pro_id) ProfileCount,");
                sbQuery.AppendLine("  SUM(prs_profile_score.prs_total_listing_count) ListingCount,");
                sbQuery.AppendLine("  SUM(prs_profile_score.prs_actual_listing_count) ActualListingCount,");
                sbQuery.AppendLine("  SUM(prs_profile_score.prs_issue_count) IssueCount");
            }
            #endregion

            #region From
            //from
            sbQuery.AppendLine("FROM");
            sbQuery.AppendLine(string.Format("  {0}prs_profile_score", Schema));
            sbQuery.AppendLine(string.Format("  INNER JOIN {0}pro_profile ON prs_profile_score.prs_pro_id = pro_profile.pro_id", Schema));

            //this is to keep track of which dimensions have already been joined
            var lstJoinedDimensions = new List<string>();
            lstJoinedDimensions.Add("pro_profile");

            //if group by is being used, must add the required dimension
            if (req.GroupBy != null)
            {
                switch (req.GroupBy.Dimension.ToLower())
                {
                    case "dom_domain":
                        //need to go through listings here, can't avoid it
                        sbQuery.AppendLine(string.Format("  INNER JOIN {0}lis_listing_score ON prs_profile_score.prs_pro_id = lis_listing_score.lis_pro_id", Schema));
                        sbQuery.AppendLine("                                 AND prs_profile_score.prs_tim_date = lis_listing_score.lis_tim_date");
                        sbQuery.AppendLine(string.Format("  INNER JOIN {0}dom_domain ON dom_domain.dom_id = lis_listing_score.lis_dom_id", Schema));
                        break;

                    case "mic_micro_market":
                        //KEEP THIS LEFT JOINED! not all profiles have a polygon
                        //pol_polygon may have been already added, check first!
                        if (!lstJoinedDimensions.Contains("pol_polygon"))
                        {
                            sbQuery.AppendLine(string.Format("  LEFT JOIN {0}pol_polygon ON prs_profile_score.prs_pol_id = pol_polygon.pol_id", Schema));
                            lstJoinedDimensions.Add("pol_polygon");
                        }
                        sbQuery.AppendLine(string.Format(" LEFT JOIN {0}mic_micro_market ON pol_polygon.pol_mic_id = mic_micro_market.mic_id", Schema));
                        break;

                    case "pro_profile":
                        //do nothing, already joined above
                        break;

                    case "tim_time":
                        sbQuery.AppendLine(string.Format("  INNER JOIN {0}tim_time ON prs_profile_score.prs_tim_date = tim_time.tim_date", Schema));
                        break;

                    case "pol_polygon":
                        //KEEP THIS LEFT JOINED! not all profiles have a polygon
                        //probably this will never be used on its own, but here it is anyway
                        sbQuery.AppendLine(string.Format("  LEFT JOIN {0}pol_polygon ON pro_profile.pro_pol_id = pol_polygon.pol_id", Schema));
                        break;

                    case "lis_listing_score":
                    case "lsd_listing_score_detail":
                    case "prs_profile_score":
                    case "pog_polygon_geometry":
                        throw new WrongDimensionException(req.GroupBy.Dimension, req.GroupBy.ColumnName, string.Format("Not a dimension: [{0}.{1}].", req.GroupBy.Dimension, req.GroupBy.ColumnName));

                    default:
                        throw new WrongDimensionException(req.GroupBy.Dimension, req.GroupBy.ColumnName, string.Format("Unknown dimension reference: [{0}.{1}].", req.GroupBy.Dimension, req.GroupBy.ColumnName));
                }

                //add it to the already-joined list
                lstJoinedDimensions.Add(req.GroupBy.Dimension.ToLower());
            }

            //where
            if (req.DimensionFilters!= null)
            {
                //first add required dimensions, if not already added
                foreach (var filter in req.DimensionFilters)
                {
                    if (!lstJoinedDimensions.Contains(filter.Dimension.ToLower()))
                    {
                        switch (filter.Dimension.ToLower())
                        {
                            case "dom_domain":
                                //need to go through listings here, can't avoid it
                                sbQuery.AppendLine(string.Format("  INNER JOIN {0}lis_listing_score ON prs_profile_score.prs_pro_id = lis_listing_score.lis_pro_id", Schema));
                                sbQuery.AppendLine("                                 AND prs_profile_score.prs_tim_date = lis_listing_score.lis_tim_date");
                                sbQuery.AppendLine(string.Format("  INNER JOIN {0}dom_domain ON dom_domain.dom_id = lis_listing_score.lis_dom_id", Schema));
                                break;

                            case "mic_micro_market":
                                //KEEP THIS LEFT JOINED! not all profiles have a polygon
                                //pol_polygon may have been already added, check first!
                                if (!lstJoinedDimensions.Contains("pol_polygon"))
                                {
                                    sbQuery.AppendLine(string.Format("  LEFT JOIN {0}pol_polygon ON pro_profile.pro_pol_id = pol_polygon.pol_id", Schema));
                                    lstJoinedDimensions.Add("pol_polygon");
                                }
                                sbQuery.AppendLine(string.Format(" LEFT JOIN {0}mic_micro_market ON pol_polygon.pol_mic_id = mic_micro_market.mic_id", Schema));
                                break;

                            case "pro_profile":
                                //do nothing, already joined above
                                break;

                            case "tim_time":
                                sbQuery.AppendLine(string.Format("  INNER JOIN {0}tim_time ON prs_profile_score.prs_tim_date = tim_time.tim_date", Schema));
                                break;

                            case "pol_polygon":
                                //KEEP THIS LEFT JOINED! not all profiles have a polygon
                                //probably this will never be used on its own, but here it is anyway
                                sbQuery.AppendLine(string.Format("  LEFT JOIN {0}pol_polygon ON pro_profile.pro_pol_id = pol_polygon.pol_id", Schema));
                                break;

                            case "lis_listing_score":
                            case "lsd_listing_score_detail":
                            case "prs_profile_score":
                            case "pog_polygon_geometry":
                                throw new WrongDimensionException(filter.Dimension, filter.ColumnName, string.Format("Not a dimension: [{0}.{1}].", filter.Dimension, filter.ColumnName));

                            default:
                                throw new WrongDimensionException(filter.Dimension, filter.ColumnName, string.Format("Unknown dimension reference: [{0}.{1}].", filter.Dimension, filter.ColumnName));
                        }
                        lstJoinedDimensions.Add(filter.Dimension.ToLower());
                    }
                }
            }
            #endregion

            #region Where
            sbQuery.AppendLine("WHERE");
            //always must filter by current directory_id (given by token in the request)
            sbQuery.AppendLine(string.Format("  pro_profile.pro_directory_id = {0}", DirectoryID));

            //now add the dimension filters
            if (req.DimensionFilters != null)
            {
                foreach (var filter in req.DimensionFilters)
                {
                    sbQuery.AppendLine(string.Format("  AND {0}.{1} IN ({2})", filter.Dimension, filter.ColumnName, filter.Values.ToCsv()));
                }
            }
            #endregion

            #region Group By
            //group by, only if there's a group by in the request
            if (req.GroupBy != null)
            {
                sbQuery.AppendLine("GROUP BY");
                //add requested column
                sbQuery.AppendLine(string.Format("  {0}.{1}", req.GroupBy.Dimension, req.GroupBy.ColumnName));
                //also add id column if needed/useful
                if (strIdField != string.Empty)
                {
                    sbQuery.AppendLine(string.Format("  ,{0}.{1}", req.GroupBy.Dimension, strIdField));
                }

                //if group by domain, we need to order by domain weight, so we need to include it
                if (req.GroupBy.Dimension.ToLower() == "dom_domain")
                {
                    sbQuery.AppendLine(string.Format("  ,dom_domain.dom_weight", req.GroupBy.Dimension, strIdField)); 
                }

                //QUESTION: what if we don't want ORDER BY? SortByAscending == null?
                //leaving this for future consideration, as current operation works as expected this way
                //add order by if required (group by domain always needs it)
                if (req.GroupBy.Order != SortOrder.NONE || req.GroupBy.Dimension.ToLower() == "dom_domain" || req.OrderBy != null)
                {
                    sbQuery.AppendLine("ORDER BY");
                    sbQuery.AppendLine("  1 = 1"); //this is to make it easier to add the other conditions later
                }

                if (req.OrderBy != null)
                {
                    //had to add a new item called "None" in the ProfileScoreOrderColumn enum
                    //so far this is only used here, note the "Default" case is removed and it means there's no
                    //special sort is applied; this is used to recover from a logic bug when grouping by domain and
                    //no order by is used, which must result in a sort by domain weight
                    switch (req.OrderBy.Column)
                    {
                        case MetricsOrderColumn.IssueCount:
                            sbQuery.AppendLine(string.Format("  ,IssueCount {0}", req.OrderBy.Ascending ? "ASC" : "DESC"));
                            break;
                        case MetricsOrderColumn.TotalListingCount:
                            sbQuery.AppendLine(string.Format("  ,ListingCount {0}", req.OrderBy.Ascending ? "ASC" : "DESC"));
                            break;
                        case MetricsOrderColumn.ActualListingCount:
                            sbQuery.AppendLine(string.Format("  ,ActualListingCount {0}", req.OrderBy.Ascending ? "ASC" : "DESC"));
                            break;
                        case MetricsOrderColumn.ProfileScore:
                            if ((req.GroupBy != null && req.GroupBy.Dimension.ToLower() == "dom_domain")
                                || (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain")))
                            {
                                sbQuery.AppendLine(string.Format("  ,AVG(ISNULL(lis_listing_score.lis_correctness, 0)) {0}", req.OrderBy.Ascending ? "ASC" : "DESC"));
                            }
                            else
                            {
                                sbQuery.AppendLine(string.Format("  ,AVG(prs_profile_score.prs_score) {0}", req.OrderBy.Ascending ? "ASC" : "DESC"));
                            }
                            break;
                        case MetricsOrderColumn.ProfileScoreDelta:
                            if ((req.GroupBy != null && req.GroupBy.Dimension.ToLower() == "dom_domain")
                                || (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain")))
                            {
                                sbQuery.AppendLine(string.Format("  ,AVG(ISNULL(lis_listing_score.lis_correctness_delta, 0)) {0} NULLS LAST", req.OrderBy.Ascending ? "ASC" : "DESC"));
                            }
                            else
                            {
                                sbQuery.AppendLine(string.Format("  ,AVG(prs_profile_score.prs_score_delta) {0} NULLS LAST", req.OrderBy.Ascending ? "ASC" : "DESC"));
                            }
                            break;
                    }
                }

                if (req.GroupBy.Order != SortOrder.NONE || req.GroupBy.Dimension.ToLower() == "dom_domain")
                {
                    if (req.GroupBy.Dimension.ToLower() == "dom_domain")
                    {
                        sbQuery.AppendLine(string.Format("  ,dom_domain.dom_weight DESC{0}", req.GroupBy.Order != SortOrder.NONE && req.GroupBy.Dimension.ToLower() == "dom_domain" ? "," : string.Empty));
                    }

                    if (req.GroupBy.Order != SortOrder.NONE)
                    {
                        sbQuery.AppendLine(string.Format("  ,{0}.{1} {2}", req.GroupBy.Dimension, req.GroupBy.ColumnName, req.GroupBy.Order.ToString()));
                    }
                }
            }
            #endregion

            //gotta close the instruction
            sbQuery.Append(";");

            return sbQuery.ToString();
        }

        private MetricsResponse MapToMetrics(DataTable table, MetricsRequest req)
        {
            var resp = new MetricsResponse();

            if (table.Rows.Count == 1 && table.Rows[0]["HealthScore"] == DBNull.Value)
            {
                //means no data matched the request, no need to continue
                return resp;
            }

            if (table.Rows.Count > 0)
            {
                resp.Summary = new Metrics()
                {
                    HealthScore = Convert.ToDecimal(table.Compute("Avg(HealthScore)", string.Empty)),
                    HealthScoreDelta = table.Compute("Avg(HealthScoreDelta)", string.Empty) != DBNull.Value ? (decimal?)Convert.ToDecimal(table.Compute("Avg(HealthScoreDelta)", string.Empty)) : null,
                    IssueCount = Convert.ToInt32(table.Compute("Sum(IssueCount)", string.Empty)),
                    ListingCount = Convert.ToInt32(table.Compute("Sum(ListingCount)", string.Empty)),
                    ProfileCount = Convert.ToInt32(table.Compute("Sum(ProfileCount)", string.Empty)),
                    ActualListingsFoundCount = Convert.ToInt32(table.Compute("Sum(ActualListingCount)", string.Empty))
                };

                if (req.GroupBy != null)
                {
                    resp.Items = new List<Item>();

                    for (int i = 0; i < table.Rows.Count; i++)
                    {
                        resp.Items.Add(new Item()
                        {
                            Name = table.Rows[i]["Name"].ToString(),
                            ID = table.Columns.Contains("ID") ? table.Rows[i]["ID"].ToString() : null,
                            GroupTotal = new Metrics()
                            {
                                HealthScore = Convert.ToDecimal(table.Rows[i]["HealthScore"]),
                                HealthScoreDelta = table.Rows[i]["HealthScoreDelta"] != DBNull.Value ? (decimal?)Convert.ToDecimal(table.Rows[i]["HealthScoreDelta"]) : null,
                                IssueCount = Convert.ToInt32(table.Rows[i]["IssueCount"]),
                                ListingCount = Convert.ToInt32(table.Rows[i]["ListingCount"]),
                                ProfileCount = Convert.ToInt32(table.Rows[i]["ProfileCount"]),
                                ActualListingsFoundCount = Convert.ToInt32(table.Rows[i]["ActualListingCount"])
                            }
                        });
                    }
                }
            }

            return resp;
        }
        #endregion

        #region Profiles
        public ProfileResponse GetProfiles(ProfileRequest req, int DirectoryID)
        {
            var conn = new OdbcConnection(Utils.GetRedshiftConnectionString());
            var ds = new DataSet();
            var dt = new DataTable();

            try
            {
                conn.Open();
                OdbcDataAdapter da = new OdbcDataAdapter(GetProfilesQuery(req, DirectoryID), conn);
                da.Fill(ds);

                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }

            var resp = MapToProfiles(dt, req);

            resp.Status = new Status(true);

            return resp;
        }

        private string GetProfilesQuery(ProfileRequest req, int DirectoryID)
        {
            var sbQuery = new StringBuilder();
            var lstJoinedDimensions = new List<string>();

            sbQuery.AppendLine("SELECT");
            sbQuery.AppendLine("  pro_id");

            #region Requested columns
            //only show more columns if requested
            if (req.Columns != null && req.Columns.Count > 0)
            {
                foreach (var column in req.Columns.Where(x => x != "pro_id"))
                {
                    sbQuery.AppendLine(string.Format("  ,{0}", column));

                    switch (column)
                    {
                        case "mic_id":
                        case "mic_name":
                            if (req.DimensionFilters == null)
                            {
                                req.DimensionFilters = new List<Models.DataQuery.Attribute>();
                            }
                            if (req.DimensionFilters.Find(x => x.Dimension.ToLower() == "mic_micro_market") == null)
                            {
                                req.DimensionFilters.Add(new Models.DataQuery.Attribute() { Dimension = "mic_micro_market" });
                            }
                            break;
                        case "pol_id":
                        case "pol_area_name":
                            if (req.DimensionFilters == null)
                            {
                                req.DimensionFilters = new List<Models.DataQuery.Attribute>();
                            }
                            if (req.DimensionFilters.Find(x => x.Dimension.ToLower() == "pol_polygon") == null)
                            {
                                req.DimensionFilters.Add(new Models.DataQuery.Attribute() { Dimension = "pol_polygon" });
                            }
                            break;
                    }
                }
            }
            #endregion

            #region From
            //FROM
            sbQuery.AppendLine("FROM");
            sbQuery.AppendLine(string.Format("  {0}pro_profile", Schema));
            
            //EXTRA JOINS FROM REQUEST QUERY HERE
            if (req.DimensionFilters != null)
            {
                //first add required dimensions, if not already added
                foreach (var filter in req.DimensionFilters)
                {
                    if (!lstJoinedDimensions.Contains(filter.Dimension.ToLower()))
                    {
                        switch (filter.Dimension.ToLower())
                        {
                            case "dom_domain":
                                //do nothing, can't join
                                break;

                            case "mic_micro_market":
                                //pol_polygon may have been already added, check first!
                                if (!lstJoinedDimensions.Contains("pol_polygon"))
                                {
                                    sbQuery.AppendLine(string.Format("    LEFT JOIN {0}pol_polygon ON pol_polygon.pol_id = pro_profile.pro_pol_id", Schema));
                                    lstJoinedDimensions.Add("pol_polygon");
                                }
                                sbQuery.AppendLine(string.Format("    LEFT JOIN {0}mic_micro_market ON mic_micro_market.mic_id = pol_polygon.pol_mic_id", Schema));
                                break;

                            case "pro_profile":
                                //do nothing, already included above
                                break;

                            case "tim_time":
                                //do nothing, can't join
                                break;

                            case "pol_polygon":
                                sbQuery.AppendLine(string.Format("    LEFT JOIN {0}pol_polygon ON pol_polygon.pol_id = pro_profile.pro_pol_id", Schema));
                                break;

                            case "lis_listing_score":
                            case "lsd_listing_score_detail":
                            case "prs_profile_score":
                            case "pog_polygon_geometry":
                                throw new WrongDimensionException(filter.Dimension, filter.ColumnName, string.Format("Not a dimension: [{0}.{1}].", filter.Dimension, filter.ColumnName));

                            default:
                                throw new WrongDimensionException(filter.Dimension, filter.ColumnName, string.Format("Unknown dimension reference: [{0}.{1}].", filter.Dimension, filter.ColumnName));
                        }
                        lstJoinedDimensions.Add(filter.Dimension.ToLower());
                    }
                }
            }
            #endregion

            #region Where
            //WHERE
            sbQuery.AppendLine("  WHERE");
            sbQuery.AppendLine(string.Format("      pro_profile.pro_directory_id = {0}", DirectoryID));

            if (req.DimensionFilters != null)
            {
                foreach (var filter in req.DimensionFilters)
                {
                    if (filter.Values != null && filter.Values.Count > 0)
                    {
                        sbQuery.AppendLine(string.Format("	    AND {0}.{1} IN ({2})", filter.Dimension, filter.ColumnName, filter.Values.ToCsv()));
                    }
                }
            }
            #endregion

            #region Order By
            //order by, if requested
            if (req.OrderBy != null && req.OrderBy.Count > 0)
            {
                sbQuery.AppendLine("ORDER BY");
                string OrderBy = string.Empty;
                foreach (var column in req.Columns)
                {
                    OrderBy += string.Format(",{0}", column);
                }
                sbQuery.AppendLine(string.Format("  {0}", OrderBy.Substring(1)));
            }
            #endregion

            //gotta close the instruction
            sbQuery.Append(";");

            return sbQuery.ToString();
        }

        private ProfileResponse MapToProfiles(DataTable table, ProfileRequest req)
        {
            var resp = new ProfileResponse();
            resp.Profiles = new List<Profile>();
            var profile = new Profile();

            for (int i = 0; i < table.Rows.Count; i++)
            {
                //pro_id must always be there
                profile = new Profile() { ID = Convert.ToInt64(table.Rows[i]["pro_id"]) };

                foreach (var column in req.Columns)
                {
                    //switch by column and feed proper data to class property
                    #region Switch By Column
                    switch (column)
                    {
                        case "pro_name":
                            if (profile.NAPInfo == null)
                            {
                                profile.NAPInfo = new NAPInfo();
                            }
                            profile.NAPInfo.Name = table.Rows[i][column].ToString();
                            break;
                        case "pro_address":
                            if (profile.NAPInfo == null)
                            {
                                profile.NAPInfo = new NAPInfo();
                            }
                            if (profile.NAPInfo.Address == null)
                            {
                                profile.NAPInfo.Address = new Address();
                            }
                            profile.NAPInfo.Address.Address1 = table.Rows[i][column].ToString();
                            break;
                        case "pro_city":
                            if (profile.NAPInfo == null)
                            {
                                profile.NAPInfo = new NAPInfo();
                            }
                            if (profile.NAPInfo.Address == null)
                            {
                                profile.NAPInfo.Address = new Address();
                            }
                            profile.NAPInfo.Address.City = table.Rows[i][column].ToString();
                            break;
                        case "pro_state":
                            if (profile.NAPInfo == null)
                            {
                                profile.NAPInfo = new NAPInfo();
                            }
                            if (profile.NAPInfo.Address == null)
                            {
                                profile.NAPInfo.Address = new Address();
                            }
                            profile.NAPInfo.Address.State = table.Rows[i][column].ToString();
                            break;
                        case "pro_zip_code":
                            if (profile.NAPInfo == null)
                            {
                                profile.NAPInfo = new NAPInfo();
                            }
                            if (profile.NAPInfo.Address == null)
                            {
                                profile.NAPInfo.Address = new Address();
                            }
                            profile.NAPInfo.Address.ZipCode = table.Rows[i][column].ToString();
                            break;
                        case "pro_phone":
                            if (profile.NAPInfo == null)
                            {
                                profile.NAPInfo = new NAPInfo();
                            }
                            profile.NAPInfo.Phone = table.Rows[i][column].ToString();
                            break;
                        case "pro_website":
                            if (profile.NAPInfo == null)
                            {
                                profile.NAPInfo = new NAPInfo();
                            }
                            profile.NAPInfo.WebSite = table.Rows[i][column].ToString();
                            break;
                        case "pro_directory_name":
                            profile.DirectoryName = table.Rows[i][column].ToString();
                            break;
                        case "pro_directory_id":
                            profile.DirectoryID = Convert.ToInt64(table.Rows[i][column].ToString());
                            break;
                        case "pro_latitude":
                            if (profile.GeoLocation == null)
                            {
                                profile.GeoLocation = new GeoLocation();
                            }
                            //need to force Culture on lat/lon so convert.todecimal won't fail when running in different regional settings
                            profile.GeoLocation.Latitude = (decimal?)Convert.ToDecimal(table.Rows[i][column], new CultureInfo("en-US"));
                            break;
                        case "pro_longitude":
                            if (profile.GeoLocation == null)
                            {
                                profile.GeoLocation = new GeoLocation();
                            }
                            //need to force Culture on lat/lon so convert.todecimal won't fail when running in different regional settings
                            profile.GeoLocation.Longitude = (decimal?)Convert.ToDecimal(table.Rows[i][column], new CultureInfo("en-US"));
                            break;
                        case "pol_id":
                            if (table.Rows[i][column] != DBNull.Value)
                            {
                                if (profile.Polygon == null)
                                {
                                    profile.Polygon = new Polygon();
                                }
                                profile.Polygon.ID = Convert.ToInt64(table.Rows[i][column].ToString());
                            }
                            break;
                        case "pol_area_name":
                            if (table.Rows[i][column] != DBNull.Value)
                            {
                                if (profile.Polygon == null)
                                {
                                    profile.Polygon = new Polygon();
                                }
                                profile.Polygon.AreaName = table.Rows[i][column].ToString();
                            }
                            break;
                        case "mic_id":
                            if (table.Rows[i][column] != DBNull.Value)
                            {
                                if (profile.Polygon == null)
                                {
                                    profile.Polygon = new Polygon();
                                }
                                if (profile.Polygon.MicroMarket == null)
                                {
                                    profile.Polygon.MicroMarket = new Micromarket();
                                }
                                profile.Polygon.MicroMarket.ID = Convert.ToInt64(table.Rows[i][column].ToString());
                            }
                            break;
                        case "mic_name":
                            if (table.Rows[i][column] != DBNull.Value)
                            {
                                if (profile.Polygon == null)
                                {
                                    profile.Polygon = new Polygon();
                                }
                                if (profile.Polygon.MicroMarket == null)
                                {
                                    profile.Polygon.MicroMarket = new Micromarket();
                                }
                                profile.Polygon.MicroMarket.Name = table.Rows[i][column].ToString();
                            }
                            break;
                            
                    }
                    #endregion
                }

                resp.Profiles.Add(profile);
            }

            return resp;
        }
        #endregion

        #region ProfileScores
        public ListingResponse GetProfileScores(ProfileScoreRequest req, int DirectoryID)
        {
            var dtListings = new DataTable();
            var dtProfilesSummary = new DataTable();

            try
            {
                OdbcDataAdapter daProfileScores = new OdbcDataAdapter(GetProfileScoresQuery(req, DirectoryID), Utils.GetRedshiftConnectionString());
                OdbcDataAdapter daProfilesSummary = new OdbcDataAdapter(GetProfileScoresSummaryQuery(req, DirectoryID), Utils.GetRedshiftConnectionString());
                
                //sync code for testing
                //daProfileScores.Fill(dtListings);
                //daProfilesSummary.Fill(dtProfilesSummary);
                
                //async code for prod
                List<Task> queries = new List<Task>()
                {
                    Task.Factory.StartNew(() => { daProfileScores.Fill(dtListings); }),
                    Task.Factory.StartNew(() => { daProfilesSummary.Fill(dtProfilesSummary); })
                };

                Task.WaitAll(queries.ToArray());
            }
            catch (Exception ex)
            {
                throw;
            }

            var resp = MapToProfileScores(dtListings, dtProfilesSummary, req);

            resp.Status = new Status(true);

            return resp;
        }

        private string GetProfileScoresQuery(ProfileScoreRequest req, int DirectoryID)
        {
            var sbQuery = new StringBuilder();
            //this is to keep track of which dimensions have already been joined
            var lstJoinedDimensions = new List<string>();

            sbQuery.AppendLine("WITH profiles AS (");
            sbQuery.AppendLine("  SELECT");

            #region Row Number
            //WARNING!! PAGING ORDER MUST PREVAIL!
            //ORDER BY FOR DENSE_RANK() SHOULD BE THE SAME AS THE FINAL SELECT DOWN BELOW, MINUS ONE LEVEL
            // i.e.:
            //  if select down below uses "order by date, score, id, domain"
            //  then order by up here should be "order by date, score, id" (minus domain)
            var OrderColumn = string.Empty;
            switch (req.OrderBy.Column)
            {
                case ProfileScoreOrderColumn.Score:
                default:
                    OrderColumn = string.Format("prs_profile_score.prs_score {0}", req.OrderBy.Ascending ? "ASC" : "DESC");
                    break;
                case ProfileScoreOrderColumn.ScoreDelta:
                    OrderColumn = string.Format("prs_profile_score.prs_score_delta {0} NULLS LAST", req.OrderBy.Ascending ? "ASC" : "DESC");
                    break;
            }
            if (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain"))
            {
                sbQuery.AppendLine(string.Format("    DENSE_RANK() OVER(ORDER BY prs_profile_score.prs_tim_date, {0}, prs_profile_score.prs_pro_id, dom_domain.dom_weight DESC) AS row_nbr,", OrderColumn));
            }
            else
            {
                sbQuery.AppendLine(string.Format("    DENSE_RANK() OVER(ORDER BY prs_profile_score.prs_tim_date, {0}, prs_profile_score.prs_pro_id) AS row_nbr,", OrderColumn));
            }
            #endregion

            #region Profile Info
            sbQuery.AppendLine("    --PROFILE SCORING");
            sbQuery.AppendLine("    prs_profile_score.prs_score,");
            sbQuery.AppendLine("    prs_profile_score.prs_score_delta,");
            sbQuery.AppendLine("    prs_profile_score.prs_total_listing_count,");
            sbQuery.AppendLine("    prs_profile_score.prs_actual_listing_count,");
            sbQuery.AppendLine("    prs_profile_score.prs_issue_count,");

            if (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain"))
            {
                sbQuery.AppendLine("    AVG(lis_listing_score.lis_correctness) OVER(PARTITION BY 1) ProfileHealthScoreByDomain,");
                sbQuery.AppendLine("    AVG(lis_listing_score.lis_correctness_delta) OVER(PARTITION BY 1) ProfileHealthScoreDeltaByDomain,");
                sbQuery.AppendLine("    dom_domain.dom_name,");
            }

            sbQuery.AppendLine("    pro_profile.pro_id,");
            sbQuery.AppendLine("    pro_profile.pro_name,");
            sbQuery.AppendLine("    pro_profile.pro_address,");
            sbQuery.AppendLine("    pro_profile.pro_city,");
            sbQuery.AppendLine("    pro_profile.pro_state,");
            sbQuery.AppendLine("    pro_profile.pro_zip_code,");
            sbQuery.AppendLine("    pro_profile.pro_phone,");
            sbQuery.AppendLine("    pro_profile.pro_website,");
            sbQuery.AppendLine("    pro_profile.pro_directory_id,");
            sbQuery.AppendLine("    pro_profile.pro_latitude,");
            sbQuery.AppendLine("    pro_profile.pro_longitude");
            #endregion

            #region From
            sbQuery.AppendLine("  FROM");
            sbQuery.AppendLine(string.Format("    {0}prs_profile_score", Schema));
            sbQuery.AppendLine(string.Format("    INNER JOIN {0}pro_profile ON prs_profile_score.prs_pro_id = pro_profile.pro_id", Schema));

            //keep track of joined dimensions
            lstJoinedDimensions.Add("pro_profile");

            //EXTRA JOINS FROM REQUEST QUERY HERE
            if (req.DimensionFilters != null)
            {
                //first add required dimensions, if not already added
                foreach (var filter in req.DimensionFilters)
                {
                    if (!lstJoinedDimensions.Contains(filter.Dimension.ToLower()))
                    {
                        switch (filter.Dimension.ToLower())
                        {
                            case "dom_domain":
                                if (!lstJoinedDimensions.Contains("lis_listing_score"))
                                {
                                    sbQuery.AppendLine(string.Format("  INNER JOIN {0}lis_listing_score ON lis_listing_score.lis_pro_id = prs_profile_score.prs_pro_id", Schema));
                                    sbQuery.AppendLine("                                  AND lis_listing_score.lis_tim_date = prs_profile_score.prs_tim_date");
                                }
                                sbQuery.AppendLine(string.Format("  INNER JOIN {0}dom_domain ON lis_listing_score.lis_dom_id = dom_domain.dom_id", Schema));
                                break;

                            case "mic_micro_market":
                                //pol_polygon may have been already added, check first!
                                if (!lstJoinedDimensions.Contains("pol_polygon"))
                                {
                                    sbQuery.AppendLine(string.Format("    LEFT JOIN {0}pol_polygon ON pol_polygon.pol_id = prs_profile_score.prs_pol_id", Schema));
                                    lstJoinedDimensions.Add("pol_polygon");
                                }
                                sbQuery.AppendLine(string.Format("    LEFT JOIN {0}mic_micro_market ON mic_micro_market.mic_id = pol_polygon.pol_mic_id", Schema));
                                break;

                            case "pro_profile":
                                //do nothing, already joined above
                                break;

                            case "tim_time":
                                sbQuery.AppendLine(string.Format("    INNER JOIN {0}tim_time ON tim_time.tim_date = prs_profile_score.prs_tim_date", Schema));
                                break;

                            case "pol_polygon":
                                sbQuery.AppendLine(string.Format("    LEFT JOIN {0}pol_polygon ON pol_polygon.pol_id = prs_profile_score.prs_pol_id", Schema));
                                break;

                            case "lis_listing_score":
                            case "lsd_listing_score_detail":
                            case "prs_profile_score":
                            case "pog_polygon_geometry":
                                throw new WrongDimensionException(filter.Dimension, filter.ColumnName, string.Format("Not a dimension: [{0}.{1}].", filter.Dimension, filter.ColumnName));

                            default:
                                throw new WrongDimensionException(filter.Dimension, filter.ColumnName, string.Format("Unknown dimension reference: [{0}.{1}].", filter.Dimension, filter.ColumnName));
                        }
                        lstJoinedDimensions.Add(filter.Dimension.ToLower());
                    }
                }
            }
            #endregion

            #region Where
            sbQuery.AppendLine("  WHERE");
            sbQuery.AppendLine(string.Format("      pro_profile.pro_directory_id = {0}", DirectoryID));

            foreach (var filter in req.DimensionFilters)
            {
                sbQuery.AppendLine(string.Format("	    AND {0}.{1} IN ({2})", filter.Dimension, filter.ColumnName, filter.Values.ToCsv()));
            }
            #endregion

            #region Order By
            //WARNING!! PAGING ORDER MUST PREVAIL!
            //DON'T ALTER THE ORDER BY! (Unless you know EXACTLY what you're doing)
            //Must guarantee same order every time, so paging works
            sbQuery.AppendLine("  ORDER BY");
            sbQuery.AppendLine("    prs_profile_score.prs_tim_date,");

            switch (req.OrderBy.Column)
            {
                case ProfileScoreOrderColumn.Score:
                default:
                    sbQuery.AppendLine(string.Format("    prs_profile_score.prs_score {0},", req.OrderBy.Ascending ? "ASC" : "DESC"));
                    break;
                case ProfileScoreOrderColumn.ScoreDelta:
                    sbQuery.AppendLine(string.Format("    prs_profile_score.prs_score_delta {0} NULLS LAST,", req.OrderBy.Ascending ? "ASC" : "DESC"));
                    break;
            }

            sbQuery.AppendLine("    prs_profile_score.prs_pro_id");

            if (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain"))
            {
                sbQuery.AppendLine("      ,dom_domain.dom_weight DESC");
            }
            #endregion

            #region External Select
            sbQuery.AppendLine("  )");
            sbQuery.AppendLine("SELECT");
            sbQuery.AppendLine("  prs_score ProfileScore,");
            sbQuery.AppendLine("  prs_score_delta ProfileScoreDelta,");
            sbQuery.AppendLine("  *");
            sbQuery.AppendLine("FROM");
            sbQuery.AppendLine("  profiles");
            sbQuery.AppendLine("WHERE");
            sbQuery.AppendLine(string.Format("  pro_directory_id = {0}", DirectoryID));

            if (req.ProfilesWithDifferencesOnly)
            {
                sbQuery.AppendLine("  AND prs_issue_count > 0");
            }

            if (req.Pagination != null && req.Pagination.Size > 0)
            {
                sbQuery.AppendLine("  AND row_nbr >= " + req.Pagination.From.ToString());
                sbQuery.AppendLine("  AND row_nbr < " + (req.Pagination.From + req.Pagination.Size).ToString());
            }

            //final sort before output
            sbQuery.AppendLine("ORDER BY");
            sbQuery.AppendLine("  row_nbr");
            #endregion

            //gotta close the instruction
            sbQuery.Append(";");

            return sbQuery.ToString();
        }

        private string GetProfileScoresSummaryQuery(ProfileScoreRequest req, int DirectoryID)
        {
            var reqListingSummary = new ListingRequest()
            {
                DimensionFilters = req.DimensionFilters,
                ListingsWithDifferencesOnly = req.ProfilesWithDifferencesOnly,
                Pagination = req.Pagination
            };

            return GetProfilesSummaryQuery(reqListingSummary, DirectoryID);
        }

        private ListingResponse MapToProfileScores(DataTable Profiles, DataTable ProfileScoresSummary, ProfileScoreRequest req)
        {
            var resp = new ListingResponse();

            resp.Pagination = req.Pagination;

            if (ProfileScoresSummary.Rows.Count > 0)
            {
                resp.Summary = new Metrics();

                //if domain is being used for filtering, things get crazy, 
                //must switch to score correctness
                if (req.DimensionFilters != null && req.DimensionFilters.Exists(x => x.Dimension == "dom_domain"))
                {
                    resp.Summary.HealthScore = Profiles.Rows.Count > 0 ? Convert.ToDecimal(Profiles.Rows[0]["ProfileHealthScoreByDomain"]) : 0m;
                    resp.Summary.HealthScoreDelta = Profiles.Rows.Count > 0 && Profiles.Rows[0]["ProfileHealthScoreDeltaByDomain"] != DBNull.Value ? (decimal?)Convert.ToDecimal(Profiles.Rows[0]["ProfileHealthScoreDeltaByDomain"]) : null;
                }
                else
                {
                    resp.Summary.HealthScore = Convert.ToDecimal(ProfileScoresSummary.Rows[0]["TotalHealthScore"]); //AVG already calculated on SQL
                    resp.Summary.HealthScoreDelta = ProfileScoresSummary.Rows[0]["TotalHealthScoreDelta"] != DBNull.Value ? (decimal?)Convert.ToDecimal(ProfileScoresSummary.Rows[0]["TotalHealthScoreDelta"]) : null;
                }
                resp.Summary.IssueCount = Convert.ToInt32(ProfileScoresSummary.Rows[0]["TotalIssueCount"]);
                resp.Summary.ListingCount = Convert.ToInt32(ProfileScoresSummary.Rows[0]["TotalListingCount"]);
                resp.Summary.ActualListingsFoundCount = Convert.ToInt32(ProfileScoresSummary.Rows[0]["ActualListingsCount"]);
                resp.Summary.ProfileCount = Convert.ToInt32(ProfileScoresSummary.Rows[0]["TotalProfileCount"]);
            }

            if (Profiles.Rows.Count > 0)
            {
                resp.Profiles = new List<Profile>();

                for (int i = 0; i < Profiles.Rows.Count; i++)
                {
                    #region Profile
                    resp.Profiles.Add(new Profile()
                    {
                        ResultsetIndex = Convert.ToInt32(Profiles.Rows[i]["row_nbr"]),
                        ID = Convert.ToInt64(Profiles.Rows[i]["pro_id"]),
                        GeoLocation = new GeoLocation()
                        {
                            //need to force Culture on lat/lon so convert.todecimal won't fail when running in different regional settings
                            Latitude = (decimal?)Convert.ToDecimal(Profiles.Rows[i]["pro_latitude"], new CultureInfo("en-US")),
                            Longitude = (decimal?)Convert.ToDecimal(Profiles.Rows[i]["pro_longitude"], new CultureInfo("en-US"))
                        },
                        NAPInfo = new NAPInfo()
                        {
                            Name = Profiles.Rows[i]["pro_name"].ToString(),
                            Address = new Address()
                            {
                                Address1 = Profiles.Rows[i]["pro_address"].ToString(),
                                City = Profiles.Rows[i]["pro_city"].ToString(),
                                State = Profiles.Rows[i]["pro_state"].ToString(),
                                ZipCode = Profiles.Rows[i]["pro_zip_code"].ToString()
                            },
                            Phone = Profiles.Rows[i]["pro_phone"].ToString(),
                            WebSite = Profiles.Rows[i]["pro_website"].ToString()
                        },
                        Metrics = new Metrics()
                        {
                            HealthScore = Convert.ToDecimal(Profiles.Rows[i]["ProfileScore"]),
                            HealthScoreDelta = Profiles.Rows[i]["ProfileScoreDelta"] != DBNull.Value ? (decimal?)Convert.ToDecimal(Profiles.Rows[i]["ProfileScoreDelta"]) : null,
                            ListingCount = Convert.ToInt32(Profiles.Rows[i]["prs_total_listing_count"]),
                            ActualListingsFoundCount = Convert.ToInt32(Profiles.Rows[i]["prs_actual_listing_count"]),
                            IssueCount = Convert.ToInt32(Profiles.Rows[i]["prs_issue_count"])
                        }
                    });
                    #endregion
                }
            }
            return resp;
        }
        #endregion
    }
}